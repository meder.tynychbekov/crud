package com.example.mitapp.serivce.impl


import com.example.mitapp.model.entity.Person
import com.example.mitapp.repository.PersonRepository
import org.junit.runner.RunWith
import org.mockito.Mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import spock.lang.Shared
import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = PersonRepository.class)
class PersonServiceImplTest extends Specification {

    @Shared
    @Autowired
    private PersonRepository personRepository


    def setup() {
        personRepository = Mock()
    }

    def "FindAll"() {
        setup:
        List<Person> personList = new ArrayList<>(Arrays.asList(
                new Person("meder", "tynychbekov", 22),
                new Person("MEDER", "TYNYCHBEKOV", 2)));
        for (Person person : personList) {
            personRepository.save(person)
        }
        List<Person> personList1 = personRepository.findAll()
        when:
        personList1.size() > 0
        then:
        thrown(NullPointerException)
    }

    def "FindById"() {
        setup:
        Person person1 = personRepository.findById(1L) as Person
        when:
        person1.getName() == "meder"
        then:
        thrown(NullPointerException)

    }

    def "Save"() {
        Person person = new Person("meder", "tynychbekov", 2)
        personRepository.save(person)

        Person person1 = personRepository.findByName("meder") as Person
        when:
        person1.getName() == "meder"
        then:
        thrown(NullPointerException)
    }

    def "Delete"() {
        def person = personRepository.findById(1L) as Person
        personRepository.delete(person)
        expect:
            person == null

    }

}
