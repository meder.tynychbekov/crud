package com.example.mitapp.repository;

import com.example.mitapp.model.entity.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void NewPersonTest() {
        personRepository.save(new Person("meder", "tynychbekov", 22));
        Person person = personRepository.findByName("meder");
        org.assertj.core.api.Assertions.assertThat(person.getId()).isGreaterThan(0L);
    }

    @Test
    public void getPersonTest() {

        Person person = personRepository.findByName("meder");
        org.assertj.core.api.Assertions.assertThat(person.getId()).isEqualTo(1L);
    }

    @Test
    public void getListOfEmployeesTest() {
        List<Person> employees = personRepository.findAll();
        org.assertj.core.api.Assertions.assertThat(employees.size()).isGreaterThan(0);
    }

    @Test
    public void updateEmployeeTest() {

        Person person = new Person();
        person.setName("meder");
        person.setLastName("tynychbekov");
        person.setAge(22);
        personRepository.save(person);

        Person person1 = personRepository.findByName("meder");
        person1.setName("MEDER");
        Person personUpdated = personRepository.save(person1);
        org.assertj.core.api.Assertions.assertThat(personUpdated.getName()).isEqualTo("MEDER");

    }

    @Test
    public void deleteEmployeeTest() {

        Person person = personRepository.findById(1L).get();

        personRepository.delete(person);
        Person person1 = null;

        Optional<Person> optionalPerson = Optional.ofNullable(personRepository.findByName("meder"));

        if (optionalPerson.isPresent()) {
            person1 = optionalPerson.get();
        }

        org.assertj.core.api.Assertions.assertThat(person1).isNull();
    }
}
