package com.example.mitapp.serivce;

import com.example.mitapp.api.payload.PersonRequest;
import com.example.mitapp.api.payload.PersonResponse;
import com.example.mitapp.model.entity.Person;

import java.util.List;

public interface PersonService {

    List<PersonResponse> findAll();

    PersonResponse findById(Long id);

    PersonResponse save(PersonRequest personRequest);

    void delete(Long id);

    PersonResponse update(Long id, PersonRequest personRequest);
}
