package com.example.mitapp.serivce.impl;

import com.example.mitapp.api.payload.PersonRequest;
import com.example.mitapp.api.payload.PersonResponse;
import com.example.mitapp.exception.NotFoundException;
import com.example.mitapp.model.entity.Person;
import com.example.mitapp.model.mapper.PersonMapper;
import com.example.mitapp.repository.PersonRepository;
import com.example.mitapp.serivce.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;

    @Override
    public List<PersonResponse> findAll() {
        return personMapper.mapToResponse(personRepository.findAll());
    }

    @Override
    public PersonResponse findById(Long id) {
        return personMapper.mapToResponse(personRepository.findById(id).orElseThrow(() ->
                new NotFoundException(String.format("Object 'person' with %d id not found!", id))));
    }

    @Override
    public PersonResponse save(PersonRequest personRequest) {
        Person person = personRepository.save(personMapper.mapToEntity(null, personRequest));
        return personMapper.mapToResponse(person);
    }

    @Override
    public void delete(Long id) {
        personRepository.deleteById(id);
    }

    @Override
    public PersonResponse update(Long id, PersonRequest personRequest) {
        Person person = personMapper.mapToEntity(id, personRequest);
        personRepository.save(person);
        return personMapper.mapToResponse(person);
    }
}
