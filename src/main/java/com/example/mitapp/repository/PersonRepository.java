package com.example.mitapp.repository;

import com.example.mitapp.model.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("from Person c where c.name =?1")
    Person findByName(String name);
}