package com.example.mitapp.api.payload;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PersonResponse {

    private Long id;
    private String name;
    private String lastName;
    private int age;
}
