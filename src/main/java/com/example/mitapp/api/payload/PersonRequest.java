package com.example.mitapp.api.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonRequest {

    private String name;
    private String lastName;
    private int age;
}
