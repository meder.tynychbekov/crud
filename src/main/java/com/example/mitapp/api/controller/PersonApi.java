package com.example.mitapp.api.controller;

import com.example.mitapp.api.payload.PersonRequest;
import com.example.mitapp.api.payload.PersonResponse;
import com.example.mitapp.serivce.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/person")
public class PersonApi {
    private final PersonService personService;


    @GetMapping
    public List<PersonResponse> findAll(){
        return personService.findAll();
    }

    @PostMapping
    public PersonResponse save(@RequestBody PersonRequest personRequest){
        return personService.save(personRequest);
    }

    @GetMapping("{id}")
    public PersonResponse findById(@PathVariable Long id){
        return personService.findById(id);
    }

    @PutMapping("{id}")
    public PersonResponse update(@PathVariable Long id, @RequestBody PersonRequest personRequest){
        return personService.update(id, personRequest);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id){
        personService.delete(id);
    }
}
