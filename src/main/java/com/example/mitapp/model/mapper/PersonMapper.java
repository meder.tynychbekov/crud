package com.example.mitapp.model.mapper;

import com.example.mitapp.api.payload.PersonRequest;
import com.example.mitapp.api.payload.PersonResponse;
import com.example.mitapp.model.entity.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonMapper {

    public Person mapToEntity(Long id, PersonRequest personRequest){
        return Person.builder()
                .id(id)
                .name(personRequest.getName())
                .lastName(personRequest.getLastName())
                .age(personRequest.getAge())
                .build();
    }

    public PersonResponse mapToResponse(Person person){
        return PersonResponse.builder()
                .id(person.getId())
                .name(person.getName())
                .lastName(person.getLastName())
                .age(person.getAge())
                .build();
    }

    public List<PersonResponse> mapToResponse(List<Person> personList){
        List<PersonResponse> personResponses = new ArrayList<>();
        for (Person person: personList){
            personResponses.add(mapToResponse(person));
        }
        return personResponses;
    }
}
